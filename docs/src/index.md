title: s4g: Stupid Simple Static Site Generator
template: page.html

# TL;DR

Installation:

```bash
pip3 install s4g --user
```

Basic usage (assuming s4g is added to your `PATH`):

```bash
s4g path/to/src path/to/dst path/to/templates
```

# Getting Started

Building a static site using s4g is simple.
Just follow these steps:

1. Create a `site` folder.
2. Inside `site`, download a [template pack](#template-packs) and name the folder `templates`.
3. Again inside `site`, create two folders `src` and `public`.
4. Put some [content](#writing-content) in a Markdown file inside `src`.
5. Run `s4g src public templates` and boom! Your page is rendered in the `public` folder.


# Writing Content

s4g supports most of the features of Github Flavored Markdown.
Metadata has to be added at the very top of the file (without any delimeters), for example,

```markdown
title: Some title
template: page.html
date: 2021-12-13
tags: a, b, c

### Some heading

Some content
```

The `title` and `date` are special metadata which determine the title of the page and date of publication (used in the index creation).
The most important meta data is the `template` field. It denotes the file (relative to your template path) which is used for rendering the page.

Extra metadata are added as `<meta>` tags in the head section.


# Project Structure

The structure discussed in the Getting Started section is not the only structure that s4g can work with.
Rather, s4g has no fixed directory structure.
It copies the structure of the source directory and compiles the Markdown files to HTML.

The only magic file is `index.md`.
Any file with the name `index.md` will have access to all the html files compiled by s4g in its directory (and subdirectories, by recursion).
One can use `index.md` to create a listing of blog posts, for example.


# Template Packs

Writing templates for s4g is very easy.
We use the [Jinja](https://jinja.palletsprojects.com/en/3.0.x/) templating engine.
You can look at the `tests/templates` folder of the [source code](https://gitlab.com/grapheo12/s4g) for sample templates.
You can include the templates folder any way you like, however, if you are like me and like to hack your way through the templates, don't use gitmodules.

Visit the template page [here](/templates.html) for a list of publicly available s4g template packs.

# Macros

s4g supports the DRY (Don't repeat yourself) principle and thus supports macros (also called shortcodes in some SSGs).
s4g macros are Python functions stored in the `macros.py` file inside your templates directory.

A sample `macros.py` can look like:

```python
global figure
def figure(src="", caption=""):
    return f"""<figure>
        <img src="{ src }" alt="{ caption }" style="width:100%" loading="lazy">
        <figcaption>{ caption }</figcaption>
    </figure>"""

global gist
def gist(src="", file=""):
    if file == "":
        return f"""<script src="https://gist.github.com/{ src }.js"></script>"""
    else:
        return f"""<script src="https://gist.github.com/{ src }.js?file={ file }"></script>"""

```

Note that you can use anything from the standard library and third party libraries (given that it is available in the same environment as s4g).
However, you can't import other files in the `macros.py` file.

To use the macros, wrap the function calls in $\lbrace \lbrace \%$ and  $\% \rbrace \rbrace$, for example: $\lbrace \lbrace \%$ `figure(src="img.jpg", caption="Some caption")` $\% \rbrace \rbrace$

Anything inside $\lbrace \lbrace \%$ and  $\% \rbrace \rbrace$ is a free-standing python environment (kinda like a line of your Python shell), use it to your advantage.

# Contributing

- WE ARE IN DIRE NEED OF **TEMPLATE PACKS**. Please add a zip/tar file archiving a template folder and add it to the `docs/public/contrib` folder of the source code, add your theme to the [template page](/templates.html) and send a merge request.

- **CODE CONTRIBUTION:** `s4g` has lot less features and no unit testing at all. Please raise an issue if you want a new feature. While writing code, keep in mind that in no way we are going to sacrifice the simplicity in code structure and the freedom in project structure of s4g. It is better to discuss the changes over an issue thread before jumping to code.