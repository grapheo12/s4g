title: Template Packs
template: page.html

| Template Name | Author | Download link | Sample site |
|---------------|--------|---------------|-------------|
| Base Template | Shubham Mishra | [link](https://gitlab.com/grapheo12/s4g/-/raw/main/docs/public/contrib/base_template.zip) | [link](https://grapheo12.in) |
